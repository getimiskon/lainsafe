# Lainsafe

## Simple file upload

## Features

* Written in perl
* Can be used as pastebin
* Or as a URL shorter
* Runs with CGI
* Tor access ;)

## Installation:

1. Configure your webserver to run CGI
2. If running nginx, set `client_max_body_size` to the max size of
   the file
2. There you go.


# lainsafecli

`lainsafecli` is the command line interface for lainsafe (and other
filesharing services)

## usage

`lainsafecli --server=http://server <file>`

(for more info see -h)

# Changes from the original

Added back references to Lain.

# Donate

Lmao no. To donate to qorg11, see [here](https://git.qorg11.net/lainsafe.git/tree/README.md).

